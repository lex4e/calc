<?php 

class Calculator {
		
	static function sum($a, $b) {
		$result = $a + $b;
		return $result;
	}

	static function diff($a, $b) {
		$result = $a - $b;
		return $result;
	}

	static function dev($a, $b) {
		$result = $a / $b;
		return $result;
	}

	static function mult($a, $b) {
		$result = $a * $b;
		return $result;
	}
	
}
